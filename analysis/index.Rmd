---
title: "Home"
site: workflowr::wflow_site
output:
  workflowr::wflow_html:
    toc: false
editor_options:
  chunk_output_type: console
---

Welcome to my research website.

workflowr doco: https://workflowr.github.io/workflowr/







* [Load](load.html) : Load and QC data
* [Some tangent](tangent.html) : I thought this was a good idea at the time.




```{r eval=FALSE}
library(workflowr)
wflow_start("workflowr_chat")
wflow_use_gitlab(username = "swatqcif", repository = "workflowr_chat")
wflow_open("analysis/load.Rmd")
wflow_open("analysis/tangent.Rmd")



# do work.
wflow_publish("analysis/tangent.Rmd")
wflow_git_push()

wflow_git_status()
# Link to inspirational example from Belinda Phipson 's github (paper details there): https://bphipson.github.io/Human_Development_snRNAseq/index.html
```







