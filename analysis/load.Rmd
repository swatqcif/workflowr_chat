---
title: "load"
author: "Sarah Williams"
date: "2022-04-04"
output: workflowr::wflow_html
editor_options:
  chunk_output_type: console
---


Some part of analysis.


```{r}
library(ggplot2)
```

```{r}
ggplot(cars, aes(x=speed, y=dist) )+
  geom_point() +
  geom_smooth(method='lm') + 
  ggtitle("Cars") + theme_bw()
```
